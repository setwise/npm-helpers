// Export all components
import Sidenav from './components/Navigation/Sidenav';
import DeleteConfirmation from './components/Utilities/Actions/DeleteConfirmation';
import DisableConfirmation from './components/Utilities/Actions/DisableConfirmation';
import EnableConfirmation from './components/Utilities/Actions/EnableConfirmation';
import RestoreConfirmation from './components/Utilities/Actions/RestoreConfirmation';
import Chart from './components/Utilities/Charts/Chart';
import Datatable from './components/Utilities/Datatable';
import Modal from './components/Utilities/Modal';
import Paginator from './components/Utilities/Paginator';

import {axios, lodash, strings, swal} from './mixins';

export {
    // Mixins
    axios, lodash, strings, swal,
    // Components
    Sidenav,
    DeleteConfirmation,
    RestoreConfirmation,
    EnableConfirmation,
    DisableConfirmation,
    Chart,
    Datatable,
    Modal,
    Paginator,
};