export default {
    filters: {
        humanize(value) {
            let sections = value.split('_');
            for (let index = 0; index < sections.length; index++) {
                sections[index] = sections[index].charAt(0).toUpperCase() + sections[index].slice(1);
            }
            return sections.join(' ');
        },
    },
};
