import axios from 'axios';

export const HttpCodes = {
    Success: {
        Ok: 200,
        Created: 201,
        Accepted: 202,
        NoContent: 204,
    },
    Redirect: {
        Permanent: 301,
        Found: 302,
    },
    ClientError: {
        BadRequest: 400,
        Unauthorized: 401,
        Forbidden: 403,
        NotFound: 404,
        Unprocessable: 422,
        Timeout: 429,
    },
};

let token = document.head.querySelector('meta[name="csrf-token"]');
if (token) {
    axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
} else {
    console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');
}

axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
axios.interceptors.response.use((response) => {
    if ('data' in response) {
        response = {
            data: response.data.data,
            message: response.data.message,
            status: response.data.status,
            headers: response.headers,
        };
    }

    return response;
}, (error) => {
    return Promise.reject(error);
});

export {
    axios,
};

export default {
    methods: {
        csrf() {
            let token = document.head.querySelector('meta[name="csrf-token"]');

            return token ? token.content : '';
        },
        axios(method = '', url = '', data = {}, config = {}) {
            let axiosPromise;

            method = method.toLowerCase();
            switch (method) {
                case 'get':
                case 'delete':
                case 'head':
                case 'options':
                    axiosPromise = axios[method](url, config);
                    break;
                case 'post':
                case 'put':
                case 'patch':
                    axiosPromise = axios[method](url, data, config);
                    break;
                default:
                    return axios;
            }

            return axiosPromise.then((response) => {
                // Any code to run on successful request

                return Promise.resolve(response);
            }).catch((error) => {
                if ('response' in error) {
                    // Redirect if unauthenticated response
                    if (error.response.status === HttpCodes.ClientError.Unauthorized) {
                    }
                    // Redirect if resource gives forbidden response
                    else if (error.response.status === HttpCodes.ClientError.Forbidden) {
                    }
                    // Redirect if resource gives page not found response
                    else if (error.response.status === HttpCodes.ClientError.NotFound) {
                    }
                    // Give cool down message on timeout code
                    else if (error.response.status === HttpCodes.ClientError.Timeout) {
                    }
                    // Validation errors occurred
                    else if (error.response.status === HttpCodes.ClientError.Unprocessable) {
                    }
                }

                return Promise.reject(error);
            }).finally(() => {
            });
        },
        get(url, config) {
            return this.axios('GET', url, {}, config);
        },
        delete(url, config) {
            return this.axios('DELETE', url, {}, config);
        },
        head(url, config) {
            return this.axios('HEAD', url, {}, config);
        },
        option(url, config) {
            return this.axios('OPTIONS', url, {}, config);
        },
        post(url, data, config) {
            return this.axios('POST', url, data, config);
        },
        patch(url, data, config) {
            return this.axios('PATCH', url, data, config);
        },
    },
};
