import _ from 'lodash';

export default {
    computed: {
        _() {
            return _;
        },
    },
    methods: {
        cloneObject(object) {
            return JSON.parse(JSON.stringify(object));
        },
    },
};
