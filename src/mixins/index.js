import _ from 'lodash';

import _axios from './axios';
import _lodash from './lodash';
import _strings from './strings';
import _swal from './swal';

const axios = _.merge({}, _axios);
const lodash = _.merge({}, _lodash);
const strings = _.merge({}, _strings);
const swal = _.merge({}, _swal);

export {
    axios,
    lodash,
    strings,
    swal,
};
