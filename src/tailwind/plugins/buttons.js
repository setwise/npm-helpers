const _ = require('lodash');
const color = require('color');

const defaultOptions = (theme) => {
    return {
        borderRadius: '.50rem',
        lineHeight: '1.25',
        fontWeight: '600',

        colors: {
            white: {
                background: theme('colors.white'),
                text: theme('colors.black'),
            },
            black: {
                background: theme('colors.black'),
                text: theme('colors.white'),
            },
            grey: {
                background: theme('colors.grey.default'),
                text: theme('colors.white'),
            },
            red: {
                background: theme('colors.red.default'),
                text: theme('colors.white'),
            },
            orange: {
                background: theme('colors.orange.default'),
                text: theme('colors.white'),
            },
            yellow: {
                background: theme('colors.yellow.default'),
                text: theme('colors.yellow.darkest'),
            },
            green: {
                background: theme('colors.green.default'),
                text: theme('colors.white'),
            },
            teal: {
                background: theme('colors.teal.default'),
                text: theme('colors.white'),
            },
            blue: {
                background: theme('colors.blue.default'),
                text: theme('colors.white'),
            },
            indigo: {
                background: theme('colors.indigo.default'),
                text: theme('colors.white'),
            },
            purple: {
                background: theme('colors.purple.default'),
                text: theme('colors.white'),
            },
            pink: {
                background: theme('colors.pink.default'),
                text: theme('colors.white'),
            },
        },

        sizes: {
            sm: {
                fontSize: '.875rem',
                padding: '0 .5rem',
            },
            default: {
                fontSize: '1rem',
                padding: '.25rem .75rem',
            },
            lg: {
                fontSize: '1.25rem',
                padding: '.5rem 1rem',
            },
        },
    };
};

module.exports = function ({addComponents, e, theme}) {
    let options = defaultOptions(theme);

    addComponents([
        {
            '.button-group': {
                'display': 'flex',
                '-webkit-user-select': 'none',
                '-moz-user-select': 'none',
                '-ms-user-select': 'none',
                'user-select': 'none',
                '-webkit-box-align': 'center',
                'align-items': 'center',
                '-webkit-box-pack': 'center',
                'justify-content': 'center',

                '& > :not(template) ~ :not(template)': {
                    '--space-x-reverse': '0',
                    'margin-right': 'calc(0.5rem * var(--space-x-reverse))',
                    'margin-left': 'calc(0.5rem * calc(1 - var(--space-x-reverse)))',
                },
            },
        },
        {
            '.button': {
                display: 'inline-block',
                textDecoration: 'none',
                boxShadow: '0 4px 6px -1px rgba(0,0,0,.1), 0 2px 4px -1px rgba(0,0,0,.06)',
                whiteSpace: 'nowrap',
                cursor: 'pointer',

                fontWeight: options.fontWeight,
                lineHeight: options.lineHeight,
                borderRadius: options.borderRadius,
                padding: options.sizes.default.padding,
                fontSize: options.sizes.default.fontSize,
            },
        },
        ..._.map(_.omit(options.sizes, 'default'), (sizeOptions, name) => {
            return {
                [`.button-${e(name)}`]: {
                    display: 'inline-block',
                    textDecoration: 'none',
                    boxShadow: '0 4px 6px -1px rgba(0,0,0,.1), 0 2px 4px -1px rgba(0,0,0,.06)',
                    whiteSpace: 'nowrap',
                    cursor: 'pointer',

                    padding: _.get(sizeOptions, 'padding', options.padding),
                    fontSize: _.get(sizeOptions, 'fontSize', options.fontSize),
                    fontWeight: _.get(sizeOptions, 'fontWeight', options.fontWeight),
                    lineHeight: _.get(sizeOptions, 'lineHeight', options.lineHeight),
                    borderRadius: _.get(sizeOptions, 'borderRadius', options.borderRadius),
                },
            };
        }),
        ..._.map(options.colors, (colorOptions, name) => {
            return {
                [`.button-${e(name)}`]: {
                    backgroundColor: colorOptions.background,
                    color: colorOptions.text,
                    '&:focus': {
                        outline: 0,
                        boxShadow: `0 0 0 .2em ${color(colorOptions.background).alpha(0.5).rgb().toString()}`,
                    },
                    '&:hover': {
                        backgroundColor: _.get(colorOptions, 'hoverBackground', color(colorOptions.background).darken(0.1).hex().toString()),
                        color: _.get(colorOptions, 'hoverText', colorOptions.text),
                    },
                    '&:active': {
                        backgroundColor: _.get(colorOptions, 'activeBackground', color(colorOptions.background).darken(0.1).hex().toString()),
                        color: _.get(colorOptions, 'activeText', colorOptions.text),
                    },
                },
            };
        }),
        ..._.map(options.colors, (colorOptions, name) => {
            return {
                [`.button-${e(name)}-transparent`]: {
                    backgroundColor: 'transparent',
                    borderWidth: '1px',
                    borderColor: color(colorOptions.background).darken(0.2).hex().toString(),
                    color: color(colorOptions.background).darken(0.2).hex().toString(),
                    '&:hover': {
                        backgroundColor: colorOptions.background,
                        borderColor: 'transparent',
                        color: theme('colors.white'),
                    },
                    '&:focus': {
                        outline: 0,
                    },
                    '&:active': {
                        outline: 0,
                    },
                },
            };
        }),
    ]);
};
